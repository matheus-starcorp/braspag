﻿using System;
using Braspag.Service.Controller;
using Braspag.Service.PagamentoService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Braspag.Models.Request;
using VendasModelV2.Models;

namespace Braspag_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagamentoController : ControllerBase
    {
        private IConfiguration _configuration;

        public PagamentoController(IConfiguration config)
        {
            _configuration = config;
        }

        [HttpPost]
        public string Pagamento([FromBody]object request)
        {
            var sale = JsonConvert.DeserializeObject<RequestControllerModel>(JsonConvert.SerializeObject(request));
            var services = new PagamentoService();
            var url = _configuration.GetSection("AppSettings").GetSection("URL_BRASPAG")?.Value;
            var response = new ResponseModel<VendaCupomModelV2>();
            try
            {
                if (sale.Config == null || sale.FormaPagamento == null || sale.VendaCupom == null)
                {
                    throw new Exception("Campos nulos");
                }

                response.ObjReturn = services.Send(sale.Config, sale.FormaPagamento, sale.VendaCupom, url, _configuration);

                response.Sucesso = true; 
                
            }
            catch (Exception ex)
            {
                response.Sucesso = false;
                response.Mensagem = ex.Message;

                return JsonConvert.SerializeObject(response);
            }

            return JsonConvert.SerializeObject(response); 
        }
    }
}